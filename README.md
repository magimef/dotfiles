Dotfiles
========

Dotfiles:

- bash: bashrc, bash_aliases, bash_profile
- git: gitconfig
- pip: pypirc
- tmux: tmux.conf
- vim: vim, vimrc
- xinit: xinitrc

Commands:

    ln -s ~/git/dotfiles/dotfiles/bashrc .bashrc
    ln -s ~/git/dotfiles/dotfiles/bash_aliases .bash_aliases
    ln -s ~/git/dotfiles/dotfiles/bash_profile .bash_profile
    ln -s ~/git/dotfiles/dotfiles/gitconfig .gitconfig
    ln -s ~/git/dotfiles/dotfiles/pypirc .pypirc
    ln -s ~/git/dotfiles/dotfiles/tmux.conf .tmux.conf
    ln -s ~/git/dotfiles/dotfiles/xinitrc .xinitrc

    ln -s ~/git/config/awesome .config/awesome
    ln -s ~/git/config/conky .config/conky

Firefox extensions:

- Tab Counter Plus: https://addons.mozilla.org/firefox/addon/tab-counter-plus
