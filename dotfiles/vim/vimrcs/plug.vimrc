" vim-plug
" https://github.com/junegunn/vim-plug
"- install: :PlugInstall
"- update: :PlugUpdate
"- clean: :PlugClean
"- status: :PlugStatus

call plug#begin()
Plug 'dpelle/vim-Grammalecte'        " grammalecte
Plug 'fatih/vim-go'                  " Go
Plug 'mhinz/vim-signify'             " diffs
Plug 'vim-airline/vim-airline'       " statusbar
Plug 'editorconfig/editorconfig-vim' " editorconfig
Plug 'dense-analysis/ale'            " syntax and lint checking
Plug 'srivathsanmurali/OpenAPIValidate.vim' " openapi validator
"Plug 'vim-syntastic/syntastic'      " syntax checking hacks for vim
"Plug 'davidhalter/jedi-vim'         " jedi: an awesome Python autocompletion
"Plug 'godlygeek/tabular'            " text filtering and alignment
call plug#end()
